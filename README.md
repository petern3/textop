# TextOp

## Summary
TOP is a game where you play as an agent’s operator/handler in a 
matrix-style world. The agent will tell you what he can see and what is 
going on, and you have to provide him with the necessary equipment and 
training to overcome the challenge. Essentially, it’s a longer version 
of the [“I need a pilot program for a B212 helicopter”](https://www.youtube.com/watch?v=6AOpomu9V6Q) from the matrix 
 possibly with the addition 
of [“I need guns, lots of guns”](https://www.youtube.com/watch?v=Y70vcs3oV14)


## Interface
This is a text based game. The agent sends you messages to tell you 
what is going on around him. This occupies most of the screen. The rest 
of the screen contains information about the agent and what you can do 
to assist your agent. 

As a suggested layout:
![Concept of interface](docs/Concept2.png)

## Interactions
Interactions with the agent can be split into three concepts. These can be implemented one by one to reduce the cost to MVP. It’s likely only the first one or perhaps two will be done for the ascii jam.
### Training the agent
### Providing the agent with supplies
### Direct Influence on the world
### Advising the agent
