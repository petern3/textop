use crate::challenges::Challenge;
use crate::equipment::Equipment;
use crate::locations::Location;
use crate::messages;
use crate::skills::Skill;

use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::path::Path;

use csv::Reader;

use heed;
use heed::types::{OwnedType, SerdeBincode, Str};
use heed::{Database, Env, EnvOpenOptions};

pub struct DataCore {
    env: Env,
    message_number: u32,
}

impl DataCore {
    pub fn new() -> Result<Self, Box<dyn Error>> {
        fs::remove_dir_all("runtime").ok();
        let path = Path::new("runtime");
        fs::create_dir_all(&path)?;
        let mut builder = EnvOpenOptions::new();
        let builder = builder.max_dbs(10).max_readers(50);
        let env = builder.open(&path)?;

        init_locations_db(&env).expect("Failed to initialize locations database");
        init_message_db(&env).expect("Failed to initialize messages database");
        init_location_connections_db(&env)
            .expect("Failed to initialize location_connections database");
        init_challenges_db(&env).expect("Failed to initialize challenges database");
        init_skills_db(&env).expect("Failed to initialize skills database");
        init_equipment_db(&env).expect("Failed to initialize equipment database");

        Ok(Self {
            env,
            message_number: 0,
        })
    }

    pub fn get_location(&self, key: &str) -> Result<Location, heed::Error> {
        let db: Database<Str, SerdeBincode<Location>> = self
            .env
            .open_database(Some("locations"))?
            .expect("Failed to open location database");
        let rtxn = self.env.read_txn()?;
        let val = db
            .get(&rtxn, key)?
            .expect(&format!("Failed to get key '{}'", key));
        Ok(val)
    }

    pub fn get_location_list(&self) -> Result<Vec<String>, heed::Error> {
        let db: Database<Str, SerdeBincode<Location>> = self
            .env
            .open_database(Some("locations"))?
            .expect("Failed to open locations database");
        let mut location_list = Vec::new();
        let rtxn = self.env.read_txn()?;
        for iter in db.iter(&rtxn)? {
            let (key, _val) = iter?;
            location_list.push(key.to_string());
        }
        Ok(location_list)
    }
    pub fn get_location_connection(&self, key: &str) -> Result<Vec<String>, heed::Error> {
        let db: Database<Str, SerdeBincode<Vec<String>>> = self
            .env
            .open_database(Some("location_connections"))?
            .expect("Failed to open location connection database");
        let rtxn = self.env.read_txn()?;
        let val = db
            .get(&rtxn, key)?
            .expect(&format!("Failed to get key '{}'", key));
        Ok(val)
    }

    pub fn get_challenge(&self, key: &str) -> Result<Challenge, heed::Error> {
        let db: Database<Str, SerdeBincode<Challenge>> = self
            .env
            .open_database(Some("challenges"))?
            .expect("Failed to open challenge database");
        let rtxn = self.env.read_txn()?;
        let val = db
            .get(&rtxn, key)?
            .expect(&format!("Failed to get key '{}'", key));
        Ok(val)
    }

    pub fn get_challenge_list(
        &self,
        _location: Option<&Location>,
    ) -> Result<Vec<String>, heed::Error> {
        // Todo: Make it filter for location
        let db: Database<Str, SerdeBincode<Challenge>> = self
            .env
            .open_database(Some("challenges"))?
            .expect("Failed to open challenge database");
        let mut challenge_list = Vec::new();
        let rtxn = self.env.read_txn()?;
        for iter in db.iter(&rtxn)? {
            let (key, _val) = iter?;
            challenge_list.push(key.to_string());
        }
        Ok(challenge_list)
    }

    pub fn get_skill(&self, key: &str) -> Result<Skill, heed::Error> {
        let db: Database<Str, SerdeBincode<Skill>> = self
            .env
            .open_database(Some("skills"))?
            .expect("Failed to open skills database");
        let rtxn = self.env.read_txn()?;
        let val = db
            .get(&rtxn, key)?
            .expect(&format!("Failed to get key '{}'", key));
        Ok(val)
    }

    pub fn get_skill_list(&self) -> Result<Vec<String>, heed::Error> {
        let db: Database<Str, SerdeBincode<Skill>> = self
            .env
            .open_database(Some("skills"))?
            .expect("Failed to open skills database");
        let mut skill_list = Vec::new();
        let rtxn = self.env.read_txn()?;
        for iter in db.iter(&rtxn)? {
            let (key, _val) = iter?;
            skill_list.push(key.to_string());
        }
        Ok(skill_list)
    }

    pub fn get_equipment(&self, key: &str) -> Result<Equipment, heed::Error> {
        let db: Database<Str, SerdeBincode<Equipment>> = self
            .env
            .open_database(Some("equipment"))?
            .expect("Failed to open equipment database");
        let rtxn = self.env.read_txn()?;
        let val = db
            .get(&rtxn, key)?
            .expect(&format!("Failed to get key '{}'", key));
        Ok(val)
    }

    pub fn get_equipment_list(&self) -> Result<Vec<String>, heed::Error> {
        let db: Database<Str, SerdeBincode<Equipment>> = self
            .env
            .open_database(Some("equipment"))?
            .expect("Failed to open equipment database");
        let mut equipment_list = Vec::new();
        let rtxn = self.env.read_txn()?;
        for iter in db.iter(&rtxn)? {
            let (key, _val) = iter?;
            equipment_list.push(key.to_string());
        }
        Ok(equipment_list)
    }

    /// Returns a list of all the messages in the database.
    pub fn get_message_list(&self) -> Result<Vec<messages::Message>, heed::Error> {
        let db: Database<OwnedType<u32>, SerdeBincode<messages::Message>> = self
            .env
            .open_database(Some("messages"))?
            .expect("Failed to open messages database");
        let mut message_list = Vec::new();
        let rtxn = self.env.read_txn()?;
        for iter in db.iter(&rtxn)? {
            let (_key, val) = iter?;
            message_list.push(val);
        }
        Ok(message_list)
    }

    pub fn add_message(&mut self, message: &messages::Message) -> Result<u32, heed::Error> {
        let db: Database<OwnedType<u32>, SerdeBincode<messages::Message>> = self
            .env
            .open_database(Some("messages"))?
            .expect("Failed to open messages database");

        let message_number = self.message_number;
        let mut wtxn = self.env.write_txn()?;
        db.put(&mut wtxn, &message_number, message)?;
        wtxn.commit()?;
        self.message_number += 1;

        return Ok(message_number);
    }
}

fn init_message_db(env: &Env) -> Result<(), Box<dyn Error>> {
    let _db: Database<OwnedType<u32>, SerdeBincode<messages::Message>> =
        env.create_database(Some("messages"))?;
    Ok(())
}

fn init_locations_db(env: &Env) -> Result<(), Box<dyn Error>> {
    let db: Database<Str, SerdeBincode<Location>> = env.create_database(Some("locations"))?;

    let data_bytes = include_bytes!("./resources/locations.csv");
    let data_str = String::from_utf8_lossy(data_bytes);
    let mut rdr = Reader::from_reader(data_str.as_bytes());
    for result in rdr.records() {
        let record = result?;
        if &record[0] != "" {
            // println!("{}", &record[0]);
            let converted_record: Location = record.deserialize(None)?;
            let mut wtxn = env.write_txn()?;
            db.put(&mut wtxn, &converted_record.name, &converted_record)?;
            wtxn.commit()?;
        }
    }
    Ok(())
}

fn init_location_connections_db(env: &Env) -> Result<(), Box<dyn Error>> {
    let data_bytes = include_bytes!("./resources/location_connections.csv");
    let data_str = String::from_utf8_lossy(data_bytes);
    let mut rdr = Reader::from_reader(data_str.as_bytes());
    let mut connection_map: HashMap<String, Vec<String>> = HashMap::new();
    for result in rdr.records() {
        let record = result?;
        if &record[0] != "" {
            // println!("{:?}", &record);
            let link_1: String = record[0].to_string();
            let link_2: String = record[1].to_string();
            if !connection_map.contains_key(&link_1) {
                let links = vec![link_2];
                connection_map.insert(link_1, links);
            } else {
                let links = connection_map
                    .get_mut(&link_1)
                    .expect(&format!("Failed to get key '{}'", link_1));
                links.push(link_2);
            }
        }
    }

    let db: Database<Str, SerdeBincode<Vec<String>>> =
        env.create_database(Some("location_connections"))?;
    for (name, links) in connection_map.iter() {
        let mut wtxn = env.write_txn()?;
        db.put(&mut wtxn, name, links)?;
        wtxn.commit()?;
    }

    Ok(())
}

fn init_encounter_history_db(env: &Env) -> Result<(), Box<dyn Error>> {
    let _db: Database<Str, SerdeBincode<Vec<String>>> =
        env.create_database(Some("location_history"))?;
    Ok(())
}

pub fn init_challenges_db(env: &Env) -> Result<(), Box<dyn Error>> {
    let db: Database<Str, SerdeBincode<Challenge>> = env.create_database(Some("challenges"))?;

    let data_bytes = include_bytes!("./resources/challenges.csv");
    let data_str = String::from_utf8_lossy(data_bytes);
    let mut rdr = Reader::from_reader(data_str.as_bytes());
    for result in rdr.records() {
        let record = result?;
        if &record[0] != "" {
            // println!("{}", &record[0]);
            let converted_record: Challenge = record.deserialize(None)?;
            let mut wtxn = env.write_txn()?;
            db.put(&mut wtxn, &converted_record.name, &converted_record)?;
            wtxn.commit()?;
        }
    }
    Ok(())
}

pub fn init_skills_db(env: &Env) -> Result<(), Box<dyn Error>> {
    let db: Database<Str, SerdeBincode<Skill>> = env.create_database(Some("skills"))?;

    let data_bytes = include_bytes!("./resources/skills.csv");
    let data_str = String::from_utf8_lossy(data_bytes);
    let mut rdr = Reader::from_reader(data_str.as_bytes());
    for result in rdr.records() {
        let record = result?;
        if &record[0] != "" {
            // println!("{}", &record[0]);
            let converted_record: Skill = record.deserialize(None)?;
            let mut wtxn = env.write_txn()?;
            db.put(&mut wtxn, &converted_record.name, &converted_record)?;
            wtxn.commit()?;
        }
    }
    Ok(())
}

pub fn init_equipment_db(env: &Env) -> Result<(), Box<dyn Error>> {
    let db: Database<Str, SerdeBincode<Equipment>> = env.create_database(Some("equipment"))?;

    let data_bytes = include_bytes!("./resources/equipment.csv");
    let data_str = String::from_utf8_lossy(data_bytes);
    let mut rdr = Reader::from_reader(data_str.as_bytes());
    for result in rdr.records() {
        let record = result?;
        if &record[0] != "" {
            // println!("{}", &record[0]);
            let converted_record: Equipment = record.deserialize(None)?;
            let mut wtxn = env.write_txn()?;
            db.put(&mut wtxn, &converted_record.name, &converted_record)?;
            wtxn.commit()?;
        }
    }
    Ok(())
}
