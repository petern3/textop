use crate::agent::Agent;
use crate::bensford;
use crate::databases::DataCore;
use crate::encounters::Encounter;
use crate::messages;
use crate::player;
use crate::skills::Skill;
use std::cmp::min;
/// The "mainloop" of the game that handles coordinating encounters,
/// challenges etc.
use std::error::Error;

use std::time::{Duration, SystemTime};

struct SkillInTraining {
    skill: Skill,
    time_of_completion: SystemTime,
}

#[derive(PartialEq)]
enum GameplayState {
    SeesNextLocation,
    SeesNextAgents,
    SeesNextChallenge,
    CompletingChallenge,
    StressedOut,
    AdrenalinedOut,
    GameOver,
}

pub struct Gameplay {
    player: Agent,
    encounters: Vec<Encounter>,
    state: GameplayState,
    update_state_time: SystemTime,
    skill_in_training: Option<SkillInTraining>,
    heartrate: u8,
    adrenaline: u8,
}

impl Gameplay {
    pub fn new(data: &DataCore) -> Result<Self, Box<dyn Error>> {
        Ok(Self {
            player: Agent::new("Player".to_string()),
            encounters: vec![Encounter::new(&data, None)?],
            state: GameplayState::SeesNextLocation,
            skill_in_training: None,
            update_state_time: SystemTime::now() + Duration::from_millis(3000),
            heartrate: 80,
            adrenaline: 20,
        })
    }

    pub fn train(&mut self, data: &mut DataCore, skill: String) {
        if self.state == GameplayState::GameOver {
            return;
        }
        if let Some(skill_in_training) = &self.skill_in_training {
            let skill = &skill_in_training.skill;
            data.add_message(&messages::Message {
                time: chrono::Utc::now(),
                message: match rand::random::<u8>() {
                    0..=127 => format!("I'm already training as {}", &skill.good_desc_en),
                    128..=255 => format!("I can't train two things at once"),
                },
                agent_stats: messages::AgentStats {
                    face: messages::Faces::SlightSmile,
                    heartrate: self.heartrate,
                    adrenaline: self.adrenaline,
                },
            })
            .expect("Failed to begin train");
        } else {
            let skill = &data.get_skill(&skill).expect("Skill doesn't exist");
            let time_to_learn = 5_000 + (bensford::get_bensford_number(100).unwrap() as u64) * 50;

            self.skill_in_training = Some(SkillInTraining {
                skill: skill.clone(),
                time_of_completion: SystemTime::now() + Duration::from_millis(time_to_learn),
            });

            let message_string: String;
            if !self.player.skills.contains(&skill) {
                message_string = format!("I have begun training to become {}", &skill.good_desc_en);
            } else {
                let current_level = self.player.skills.iter().filter(|&s| s == skill).count();
                message_string = format!(
                    "I'm taking my {} skills to level {}",
                    &skill.name,
                    current_level + 1
                );
            }
            data.add_message(&messages::Message {
                time: chrono::Utc::now(),
                message: message_string,
                agent_stats: messages::AgentStats {
                    face: messages::Faces::SlightSmile,
                    heartrate: self.heartrate,
                    adrenaline: self.adrenaline,
                },
            })
            .expect("Failed to begin train");
        }
    }
    pub fn give_equipment(&mut self, data: &mut DataCore, equipment: String) {
        if self.state == GameplayState::GameOver {
            return;
        }

        if let Some(existing_equipment) = &self.player.equipment {
            if existing_equipment.name == equipment {
                data.add_message(&messages::Message {
                    time: chrono::Utc::now(),
                    message: format!("I already have a {}", equipment).into(),
                    agent_stats: messages::AgentStats {
                        face: messages::Faces::StraightFace,
                        heartrate: self.heartrate,
                        adrenaline: self.adrenaline,
                    },
                })
                .expect("Failed to fail to give equipment");
                return;
            }
        }

        let equipment = &data
            .get_equipment(&equipment)
            .expect("Equipment doesn't exist");
        self.player.equip(Some((*equipment).clone()));
        data.add_message(&messages::Message {
            time: chrono::Utc::now(),
            message: format!("Thanks for the {}", equipment.name).into(),
            agent_stats: messages::AgentStats {
                face: messages::Faces::SlightSmile,
                heartrate: self.heartrate,
                adrenaline: self.adrenaline,
            },
        })
        .expect("Failed to give equipment");
    }

    pub fn update(&mut self, data: &mut DataCore) {
        if self.state == GameplayState::GameOver {
            return;
        }

        if let Some(skill_in_training) = &self.skill_in_training {
            if skill_in_training.time_of_completion < SystemTime::now() {
                // let speech = skill_in_training.skill.speech_en.clone();
                let skill = &skill_in_training.skill;

                let message_string: String;
                if !self.player.skills.contains(&skill) {
                    message_string = skill.speech_en.clone();
                } else {
                    let current_level = self.player.skills.iter().filter(|&s| s == skill).count();
                    message_string = format!("I am a level {} {}", current_level + 1, &skill.name);
                }
                self.player.train(skill.clone());

                data.add_message(&messages::Message {
                    time: chrono::Utc::now(),
                    message: message_string,
                    agent_stats: messages::AgentStats {
                        face: messages::Faces::SlightSmile,
                        heartrate: self.heartrate,
                        adrenaline: self.adrenaline,
                    },
                })
                .expect("Failed to train");

                self.skill_in_training = None;
            }
        }

        if self.update_state_time < SystemTime::now() {
            let mut message_string: String = "".to_string();
            let mut face = messages::Faces::StraightFace;

            let mut time_to_next = (bensford::get_bensford_number(10).unwrap() as u64) * 100;
            let mut completed_location = false;
            {
                let encounter = self.encounters.last_mut().expect("No previous encounter?!");

                match self.state {
                    GameplayState::SeesNextLocation => {
                        let location = player::location_to_string(encounter);
                        message_string = format!("I'm in {}", location);
                        self.state = GameplayState::SeesNextAgents;
                    }
                    GameplayState::SeesNextAgents => {
                        //encounters.agents.len()
                        self.state = GameplayState::SeesNextChallenge;
                    }
                    GameplayState::SeesNextChallenge => {
                        let challenge = encounter.challenges.last();
                        match challenge {
                            Some(challenge) => {
                                message_string = match rand::random::<u8>() {
                                    0..=63 => format!("  There's {}", challenge.noun_en),
                                    64..=127 => format!("  I see {} up ahead", challenge.noun_en),
                                    128..=180 => format!("  I need to {}", challenge.verb_en),
                                    181..=255 => format!("  I've got to {}", challenge.verb_en),
                                };
                            }
                            None => {}
                        }
                        self.state = GameplayState::CompletingChallenge;
                    }
                    GameplayState::CompletingChallenge => {
                        //encounters.agents.len()
                        //self.state = GameplayState::RequestItem;
                        let challenge = encounter.challenges.last();
                        match challenge {
                            Some(challenge) => {
                                message_string = match rand::random::<u8>() {
                                    0..=63 => format!("  Quickly"),
                                    64..=127 => format!("  Hurry, there's {}", challenge.noun_en),
                                    128..=180 => format!("  I need to {}", challenge.verb_en),
                                    181..=255 => format!("  I've got to {}", challenge.verb_en),
                                };

                                let mut able_to_complete = false;
                                if challenge.able_to_complete(&self.player) {
                                    able_to_complete = true;
                                }
                                if !able_to_complete && self.player.equipment.is_some() {
                                    self.player.equip_equipment();
                                    if challenge.able_to_complete(&self.player) {
                                        able_to_complete = true;
                                    } else {
                                        self.player.unequip_equipment();
                                        message_string = format!(
                                            "{}. The {} won't help",
                                            message_string,
                                            self.player.equipment.clone().expect("No equipment").name
                                        );
                                    }
                                }
                                if able_to_complete {
                                    let skill_used = challenge.completion_stat_index(&self.player);
                                    message_string = match skill_used {
                                        0 => format!(
                                            "  I managed to {} with brute force",
                                            challenge.verb_en
                                        ),
                                        1 => format!("  I jumped past {}", challenge.noun_en),
                                        2 => "  I swum through".to_string(),
                                        3 => format!("  I managed to run past {}", challenge.noun_en),
                                        4 => format!("  I could dodge {}", challenge.noun_en),
                                        5 => "  I wasn't noticed".to_string(),
                                        6 => "  I smashed 'em in hand-hand combat".to_string(),
                                        7 => "  I cut it down".to_string(),
                                        8 => format!("  I pumped {} full of lead", challenge.noun_en),
                                        9 => "  I blew it up".to_string(),
                                        10 => "  I bypassed the main controls".to_string(),
                                        11 => format!("  I got in and hacked {}", challenge.noun_en),
                                        12 => "  Vehicles, getting you through things!".to_string(),
                                        _ => "  I used magic".to_string(),
                                    };

                                    self.state = GameplayState::SeesNextChallenge;
                                    encounter.challenges.pop();

                                    // Challenge completed, agent gets some rest
                                    self.heartrate = self
                                        .heartrate
                                        .saturating_sub(bensford::get_bensford_number(20).unwrap());
                                    self.adrenaline = self
                                        .adrenaline
                                        .saturating_sub(bensford::get_bensford_number(30).unwrap());
                                    self.heartrate = min(self.heartrate, 80);
                                    self.adrenaline = min(self.adrenaline, 20);
                                } else {
                                    time_to_next += 3000; // Give the player time to do something
                                    self.heartrate = self
                                        .heartrate
                                        .saturating_add(bensford::get_bensford_number(10).unwrap());
                                    self.adrenaline = self
                                        .adrenaline
                                        .saturating_add(bensford::get_bensford_number(15).unwrap());

                                    if self.heartrate > 180 {
                                        self.state = GameplayState::StressedOut;
                                        face = messages::Faces::Grimace;
                                    } else if self.adrenaline > 180 {
                                        self.state = GameplayState::AdrenalinedOut;
                                        face = messages::Faces::Surprise;
                                    }

                                    if self.heartrate > 200 {
                                        self.state = GameplayState::StressedOut;
                                    } else if self.adrenaline > 200 {
                                        self.state = GameplayState::AdrenalinedOut;
                                    }
                                }
                                if self.player.equipment_in_use {
                                    // check stealth, maybe do something loud
                                }
                            }
                            None => {
                                // No challenges here
                                completed_location = true
                            }
                        }
                    }
                    GameplayState::StressedOut => {
                        message_string = "*** Agent collapsed from exhaustion ***".to_string();
                        face = messages::Faces::Exhausted;
                        self.state = GameplayState::GameOver;
                    }
                    GameplayState::AdrenalinedOut => {
                        message_string = "*** Agent panicked and ran away ***".to_string();
                        face = messages::Faces::Panicked;
                        self.state = GameplayState::GameOver;
                    }
                    GameplayState::GameOver => {}
                }
            }
            if completed_location {
                let next_encounter = Encounter::new(&data, self.encounters.last())
                    .expect("Couldn't create next encounter");
                self.encounters.push(next_encounter);
                face = messages::Faces::SlightSmile;
                self.state = GameplayState::SeesNextLocation;
            }

            self.update_state_time = SystemTime::now() + Duration::from_millis(time_to_next);

            if message_string != "".to_string() {
                data.add_message(&messages::Message {
                    time: chrono::Utc::now(),
                    message: message_string,
                    agent_stats: messages::AgentStats {
                        face: face,
                        heartrate: self.heartrate,
                        adrenaline: self.adrenaline,
                    },
                });
            }
        }
    }
}
