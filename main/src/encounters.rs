use std::error::Error;

use rand::seq::SliceRandom;
use rand::thread_rng;

use crate::challenges::Challenge;
use crate::databases::DataCore;
use crate::locations::Location;
use crate::locations::LocationType;
// use crate::messages::Message;
use crate::bensford::{get_bensford_letter, get_bensford_number};

#[derive(Debug)]
pub struct Encounter {
    pub location: Location,
    pub location_modifier: String,
    pub challenges: Vec<Challenge>,
    // pub agents: Vec<String>,
    // pub cover: Vec<String>,
}

impl Encounter {
    pub fn new(datacore: &DataCore, prev: Option<&Encounter>) -> Result<Self, Box<dyn Error>> {
        let (location, location_modifier) = match prev {
            Some(encounter) => Self::init_location(datacore, Some(&encounter.location))?,
            None => Self::init_location(datacore, None)?,
        };
        let challenges = Self::init_challenges(datacore, &location)?;
        // let agents = Self::init_agents(datacore, &location)?;
        // let cover = Self::init_cover(datacore, &location)?;

        Ok(Self {
            location,
            location_modifier,
            challenges,
            // agents,
            // cover,
        })
    }

    fn init_location(
        datacore: &DataCore,
        prev_location: Option<&Location>,
    ) -> Result<(Location, String), Box<dyn Error>> {
        let location_name: String = match prev_location {
            Some(location) => {
                let link_vec = datacore.get_location_connection(&location.name)?;
                let location_name = link_vec
                    .choose(&mut thread_rng())
                    .expect("Failed to choose location");
                location_name.to_string()
            }
            None => {
                let challenge_list = datacore.get_location_list()?;
                challenge_list
                    .choose(&mut thread_rng())
                    .expect("Failed to choose location")
                    .to_string()
            }
        };
        let location = datacore.get_location(&location_name)?;

        // TODO: Make sure it doesn't generate a location/modifier pair that has been recently
        let location_modifier: String = match location.category {
            LocationType::Single => "".to_string(),
            LocationType::Multi => "".to_string(),
            LocationType::MultiNum => get_bensford_number(9)
                .expect("Failed to choose location modifer")
                .to_string(),
            LocationType::MultiAl => get_bensford_letter()
                .expect("Failed to choose location modifer")
                .to_string(),
            LocationType::MultiNumAl => format!(
                "{}{}",
                get_bensford_number(9).expect("Failed to choose location modifer"),
                get_bensford_letter().expect("Failed to choose location modifer")
            ),
            LocationType::MultiNS => vec!["Northern", "Southern"]
                .choose(&mut thread_rng())
                .expect("Failed to choose location modifer")
                .to_string(),
            LocationType::MultiEW => vec!["Eastern", "Western"]
                .choose(&mut thread_rng())
                .expect("Failed to choose location modifer")
                .to_string(),
            LocationType::MultiNSEW => vec!["North", "South", "East", "West"]
                .choose(&mut thread_rng())
                .expect("Failed to choose location modifer")
                .to_string(),
        };

        Ok((location, location_modifier))
    }

    fn init_challenges(
        datacore: &DataCore,
        location: &Location,
    ) -> Result<Vec<Challenge>, Box<dyn Error>> {
        let challenge_list = datacore.get_challenge_list(Some(location))?;
        let mut challenges = Vec::with_capacity(get_bensford_number(3)?.into());
        for _num in 1..=challenges.capacity() {
            let challenge_name = challenge_list
                .choose(&mut thread_rng())
                .expect("Failed to choose location");
            challenges.push(datacore.get_challenge(challenge_name)?);
        }
        Ok(challenges)
    }

    // fn init_agents(
    //     datacore: &DataCore,
    // ) -> Result<Vec<String>, Box<dyn Error>> {
    //     unimplemented!();
    //     let links: Vec<String>;
    // }

    // fn init_cover(datacore: &DataCore, location: &Location) -> Result<Vec<String>, Box<dyn Error>> {
    //     unimplemented!();
    //     let links: Vec<String>;
    // }
}
