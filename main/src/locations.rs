use serde::{Deserialize, Serialize};

/// How the agent will specify the location
#[derive(Debug, Serialize, Deserialize)]
pub enum LocationType {
    /// There is only one
    Single,
    /// There are an indefinite number of these
    Multi,
    /// These locations are numbered 1-9 according to Bensford's law
    MultiNum,
    /// These locations are enumerated A-Z according to Bensford's law
    MultiAl,
    /// These locations are numbered 1-9 then enumerated A-Z
    MultiNumAl,
    /// These locations can be North or South
    MultiNS,
    /// These locations can be East or West
    MultiEW,
    /// These locations can be North, South, East or West
    MultiNSEW,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Location {
    pub name: String,
    pub category: LocationType,
    pub desc_en: String,
}
