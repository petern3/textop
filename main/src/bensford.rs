use rand::distributions::weighted::WeightedError;
use rand::distributions::{Distribution, WeightedIndex};
use rand::thread_rng;

pub fn get_bensford_letter() -> Result<char, WeightedError> {
    let letter_choices: [char; 26] = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    ];

    let letter_weights: [f32; 26] = [
        21.031, 12.302, 8.729, 6.770, 5.532, 4.677, 4.052, 3.574, 3.197, 2.892, 2.640, 2.429,
        2.249, 2.093, 1.958, 1.839, 1.734, 1.640, 1.556, 1.480, 1.411, 1.349, 1.291, 1.239, 1.190,
        1.145,
    ];

    let dist = WeightedIndex::new(&letter_weights)?;
    let letter = letter_choices[dist.sample(&mut thread_rng())];
    Ok(letter)
}

pub fn get_bensford_number(num: u8) -> Result<u8, WeightedError> {
    let number_choices: Vec<u8> = (1..=num).collect();
    let mut number_weights: Vec<f32> = Vec::with_capacity(num.into());
    for index in 1..=number_weights.capacity() {
        number_weights.push(100.0 * (1.0 / (index as f32) + 1.0).ln() / ((num as f32) + 1.0).ln());
    }

    let dist = WeightedIndex::new(&number_weights)?;
    let number = number_choices[dist.sample(&mut thread_rng())];
    Ok(number)
}
