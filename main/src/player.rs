use crate::encounters::Encounter;
use crate::locations::LocationType;

pub fn location_to_string(encounter: &Encounter) -> String {
    match encounter.location.category {
        LocationType::Single => format!("the {}", encounter.location.desc_en),
        LocationType::Multi => format!("a {}", encounter.location.desc_en),
        LocationType::MultiNum => format!(
            "{} {}",
            encounter.location.desc_en, encounter.location_modifier
        ),
        LocationType::MultiAl => format!(
            "{} {}",
            encounter.location.desc_en, encounter.location_modifier
        ),
        LocationType::MultiNumAl => format!(
            "{} {}",
            encounter.location.desc_en, encounter.location_modifier
        ),
        LocationType::MultiNS => format!(
            "the {} {}",
            encounter.location_modifier, encounter.location.desc_en
        ),
        LocationType::MultiEW => format!(
            "the {} {}",
            encounter.location_modifier, encounter.location.desc_en
        ),
        LocationType::MultiNSEW => format!(
            "the {} {}",
            encounter.location_modifier, encounter.location.desc_en
        ),
    }
}
