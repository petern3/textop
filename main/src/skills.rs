use serde::{Deserialize, Serialize};

use crate::agent::Stats;

/// What skills the agent can upgrade
#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum SkillType {
    /// Fighting with fists. See Ip Man, Chuck Norris, Neo
    MartialArt,
    /// Fighting with melee weapons. See Luke Skywalker, Riddick, Gandalf
    MeleeCombat,
    /// Fighting with guns. Don't see stormtroopers
    GunHandling,
    /// Fighting with explosions
    ExplosivesHandling,
    /// Operating your local carpentry woodlathe. And the sharkpit crane arm.
    MachineOperation,
    /// Hacking
    ComputerOperation,
    /// Driving or flying vehicles.
    VehicleOperation,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
#[serde(rename_all = "PascalCase")]
pub struct Skill {
    pub name: String,
    pub good_desc_en: String,
    pub bad_desc_en: String,
    pub strength: i8,
    pub jump: i8,
    pub swimming: i8,
    pub running: i8,
    pub dodge: i8,
    pub stealth: i8,
    pub martial_art: i8,
    pub melee_combat: i8,
    pub gun_handling: i8,
    pub explosives_handling: i8,
    pub machine_operation: i8,
    pub computer_operation: i8,
    pub vehicle_operation: i8,
    pub speech_en: String,
}

impl Skill {
    pub fn get_stats(&self) -> Stats {
        Stats {
            all: [
                self.strength.into(),
                self.jump.into(),
                self.swimming.into(),
                self.running.into(),
                self.dodge.into(),
                self.stealth.into(),
                self.martial_art.into(),
                self.melee_combat.into(),
                self.gun_handling.into(),
                self.explosives_handling.into(),
                self.machine_operation.into(),
                self.computer_operation.into(),
                self.vehicle_operation.into(),
            ],
        }
    }
}
