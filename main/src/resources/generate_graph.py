import csv
from collections import namedtuple
import os

Location = namedtuple("Location", ["name", "desc"])
Link = namedtuple("Link", ["a", "b"])

locations = []
links = []

with open('locations.csv', newline='') as csvfile:
    location_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    header = True
    for row in location_reader:
        if header:
            header = False
            continue
        if row[0]:
            locations.append(Location(row[0], row[2]))


with open('location_connections.csv', newline='') as csvfile:
    location_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    header = True
    for row in location_reader:
        if header:
            header = False
            continue
        if row[0]:
            links.append(Link(row[0], row[1]))



output = "digraph D {\n"
for location in locations:
    output += "    {} [label=\"{}\n({})\"];\n".format(location.name, location.desc, location.name)

for link in links:
    output += "    {} -> {};\n".format(link.a, link.b)

output += "}"

with open("locations.dot", "w") as outfile:
    outfile.write(output)

os.system("dot locations.dot -Tpng > output.png")
