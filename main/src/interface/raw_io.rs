use std::io;
use tui::backend::CrosstermBackend;
use tui::Terminal;

use crossterm::terminal::{disable_raw_mode, enable_raw_mode};

use tui::{
    layout::{Alignment, Constraint, Direction, Layout},
    style::{Color, Modifier, Style},
    widgets::{Block, BorderType, Borders, List, ListState, Paragraph, Text},
};

use super::events;
use crate::messages;

#[derive(Default)]
struct MenuState {
    pub skills_state: ListState,
    pub equipment_state: ListState,
}

pub struct RawInterface {
    terminal: Terminal<CrosstermBackend<io::Stdout>>,
    menu_state: MenuState,
}

#[derive(Debug)]
pub enum RawInterfaceAction {
    None,
    Quit(),
    TrainSkill(String),
    GiveEquipment(String),
}

impl RawInterface {
    pub fn new() -> Result<Self, std::io::Error> {
        let stdout = io::stdout();
        let backend = CrosstermBackend::new(stdout);
        let mut terminal = Terminal::new(backend)?;
        terminal.clear()?;
        terminal.hide_cursor()?;

        enable_raw_mode().expect("Couldn't enter terminal raw mode");

        Ok(Self {
            terminal,
            menu_state: MenuState::default(),
        })
    }

    pub fn update(
        &mut self,
        messages: &[messages::Message],
        equipment_items: &[String],
        skills_items: &[String],
    ) -> Result<RawInterfaceAction, std::io::Error> {
        let mut messages_text = Vec::new();
        for message in messages.iter() {
            //messages_text.push(Text::raw(&message.time));
            messages_text.push(Text::raw(message.format_message()));
        }

        let agent_stats = &messages.last().expect("No last message").agent_stats;

        let agent_text = [
            Text::raw(format!("{}\n\n", agent_stats.face.to_ascii())),
            Text::raw(format!("H: {:3} bpm\n", &agent_stats.heartrate)),
            Text::raw(format!("A:{:3} pg/ml\n", &agent_stats.adrenaline)),
        ];

        let style = Style::default().fg(Color::White).bg(Color::Black);
        let title_style = style.modifier(Modifier::BOLD);
        let border_style = style;
        let default_block = Block::default()
            .title_style(title_style)
            .style(style)
            .border_style(border_style)
            .border_type(BorderType::Rounded);

        let event = events::get_event_from_terminal().expect("Failed to get event");
        let mut scroll_amount: i32 = 0;
        let mut accept = false;
        let mut action = RawInterfaceAction::None;
        if let Some(event) = event {
            match event {
                events::RawEvent::Quit => {
                    return Ok(RawInterfaceAction::Quit());
                }
                events::RawEvent::CursorUp => {
                    scroll_amount = -1;
                }
                events::RawEvent::CursorDown => {
                    scroll_amount = 1;
                }
                events::RawEvent::Accept => {
                    accept = true;
                }
            }
        };

        let equipment_state = &mut self.menu_state.equipment_state;
        let skills_state = &mut self.menu_state.skills_state;
        if let Some(val) = skills_state.selected() {
            // If the skill menu is selected, increment it
            let new_val = val as i32 + scroll_amount;
            if new_val < 0 {
                // Overflow over the top, select bottom of equipment list
                skills_state.select(None);
                equipment_state.select(Some(equipment_items.len() - 1));
            } else if new_val == (skills_items.len()) as i32 {
                // move down to the top of the equipment list
                skills_state.select(None);
                equipment_state.select(Some(0));
            } else {
                skills_state.select(Some(new_val as usize));
                equipment_state.select(None);
            }

            if accept {
                action = RawInterfaceAction::TrainSkill(skills_items[val].to_string());
            }
        } else if let Some(val) = equipment_state.selected() {
            // If the equipment menu is selected, increment that
            let new_val = val as i32 + scroll_amount;
            if new_val < 0 {
                // Overflow over the top, select bottom of equipment list
                equipment_state.select(None);
                skills_state.select(Some(skills_items.len() - 1));
            } else if new_val == (equipment_items.len()) as i32 {
                // move down to the top of the equipment list
                equipment_state.select(None);
                skills_state.select(Some(0));
            } else {
                equipment_state.select(Some(new_val as usize));
                skills_state.select(None);
            }
            if accept {
                action = RawInterfaceAction::GiveEquipment(equipment_items[val].to_string());
            }
        } else {
            // If neither of them is selected, select one.
            skills_state.select(Some(0));
        }

        let equipment_items = equipment_items.iter().map(|i| Text::raw(i));
        let skills_items = skills_items.iter().map(|i| Text::raw(i));

        self.terminal.draw(|mut f| {
            let size = f.size();

            // Divide up the screen
            let major_division = Layout::default()
                .direction(Direction::Horizontal)
                .constraints([Constraint::Min(20), Constraint::Length(30)].as_ref())
                .split(size);

            let message_log_divison = Layout::default()
                .direction(Direction::Vertical)
                .constraints([Constraint::Min(5), Constraint::Length(5)].as_ref())
                .split(major_division[0]);

            let stats_division = Layout::default()
                .direction(Direction::Horizontal)
                .constraints(
                    [
                        Constraint::Length(12), // face
                        Constraint::Min(5),     // Latest message
                    ]
                    .as_ref(),
                )
                .split(message_log_divison[1]);

            let upgrades_division = Layout::default()
                .direction(Direction::Vertical)
                .constraints([Constraint::Percentage(50), Constraint::Percentage(50)].as_ref())
                .split(major_division[1]);

            // Boxes/borders
            let message_log_box = default_block.borders(Borders::ALL).title(" MESSAGE LOG ");

            let face_box = default_block.borders(Borders::LEFT | Borders::BOTTOM);

            let latest_message_box =
                default_block.borders(Borders::LEFT | Borders::RIGHT | Borders::BOTTOM);
            f.render_widget(latest_message_box, stats_division[1]);

            // Fill the boxes/borders
            let face = Paragraph::new(agent_text.iter())
                .block(face_box)
                .style(style)
                .wrap(true)
                .alignment(Alignment::Center);
            f.render_widget(face, stats_division[0]);

            let overflow = (messages_text.len() as u16);
            let overflow = overflow.saturating_sub(message_log_divison[0].height - 2);
            messages_text.drain(0..overflow as usize);

            let message_log = Paragraph::new(messages_text.iter())
                .block(message_log_box)
                .style(style)
                .wrap(true);
            f.render_widget(message_log, message_log_divison[0]);

            let skills_list = List::new(skills_items)
                .block(
                    default_block
                        .borders(Borders::TOP | Borders::LEFT | Borders::RIGHT)
                        .title(" SKILLS "),
                )
                .style(style)
                .highlight_style(style.modifier(Modifier::BOLD))
                .highlight_symbol("> ");
            f.render_stateful_widget(skills_list, upgrades_division[0], skills_state);

            let equipment_list = List::new(equipment_items)
                .block(
                    default_block
                        .borders(Borders::BOTTOM | Borders::LEFT | Borders::RIGHT)
                        .title(" EQUIPMENT "),
                )
                .style(style)
                .highlight_style(style.modifier(Modifier::BOLD))
                .highlight_symbol("> ");
            f.render_stateful_widget(equipment_list, upgrades_division[1], equipment_state);
        })?;
        Ok(action)
    }

    pub fn end(mut self) -> Result<(), std::io::Error> {
        disable_raw_mode().expect("Failed to exit raw mode");
        self.terminal.clear()?;
        Ok(())
    }
}
