mod events;
mod raw_io;
use std::error::Error;

use crate::databases::DataCore;

pub struct Interface {
    inter: raw_io::RawInterface,
}

pub type InterfaceAction = raw_io::RawInterfaceAction;

impl Interface {
    pub fn new() -> Result<Self, std::io::Error> {
        Ok(Self {
            inter: raw_io::RawInterface::new()?,
        })
    }

    pub fn update(&mut self, data: &mut DataCore) -> Result<InterfaceAction, Box<dyn Error>> {
        let equipment_items = data.get_equipment_list()?;
        let skills_items = data.get_skill_list()?;
        let message_list = data.get_message_list()?;

        Ok(self
            .inter
            .update(&message_list, &equipment_items, &skills_items)?)
    }

    pub fn end(self) -> Result<(), std::io::Error> {
        self.inter.end()?;
        Ok(())
    }
}
