use crossterm::event::{poll, read, Event, KeyCode, KeyEvent, KeyModifiers};
use std::time::Duration;

pub enum RawEvent {
    CursorUp,
    CursorDown,
    Accept,
    Quit,
}

pub fn get_event_from_terminal() -> Result<Option<RawEvent>, crossterm::ErrorKind> {
    if poll(Duration::from_millis(100))? {
        match read()? {
            Event::Key(event) => {
                match event {
                    KeyEvent {
                        code: KeyCode::Char('c'),
                        modifiers: KeyModifiers::CONTROL,
                    } => Ok(Some(RawEvent::Quit)),
                    KeyEvent {
                        code: KeyCode::Char('q'),
                        modifiers: KeyModifiers::NONE,
                    } => Ok(Some(RawEvent::Quit)),
                    KeyEvent {
                        code: KeyCode::Esc,
                        modifiers: KeyModifiers::NONE,
                    } => Ok(Some(RawEvent::Quit)),
                    KeyEvent {
                        code: KeyCode::Char(' '),
                        modifiers: KeyModifiers::NONE,
                    } => Ok(Some(RawEvent::Accept)),
                    KeyEvent {
                        code: KeyCode::Enter,
                        modifiers: KeyModifiers::NONE,
                    } => Ok(Some(RawEvent::Accept)),
                    KeyEvent {
                        code: KeyCode::Up,
                        modifiers: KeyModifiers::NONE,
                    } => Ok(Some(RawEvent::CursorUp)),
                    KeyEvent {
                        code: KeyCode::Down,
                        modifiers: KeyModifiers::NONE,
                    } => Ok(Some(RawEvent::CursorDown)),
                    _ => {
                        // We aren't interested in this event
                        Ok(None)
                    }
                }
            }
            _ => {
                // We aren't interested in this event
                Ok(None)
            }
        }
    } else {
        // No events in the terminal
        Ok(None)
    }
}
