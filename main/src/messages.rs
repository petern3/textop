use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

/// Messages are how the players agent communicates with the player.
/// it contains both what he looks like and biometrics at the time.
#[derive(Debug, Serialize, Deserialize)]
pub struct Message {
    pub time: DateTime<Utc>,

    pub message: String,
    pub agent_stats: AgentStats,
}

impl Message {
    /// Formats the message for display
    pub fn format_message(&self) -> String {
        format!("[ {} ] {}\n", self.time.format("%H:%M:%S"), self.message)
    }
}

/// This represents the "health" of the agent with the
/// heartate showing how tired he is, and the adrenaline
/// how much stress he is under. A high heartrate and high
/// adrenaline means your agent is likely to die.
#[derive(Debug, Serialize, Deserialize)]
pub struct AgentStats {
    pub face: Faces,
    pub heartrate: u8,  // bpm
    pub adrenaline: u8, // pg/ml
}

// The face is a little graphical representation of what the agent
// looks like.
#[allow(dead_code)]
#[derive(Debug, Serialize, Deserialize)]
pub enum Faces {
    /// At the start he has his glasses off, so he can put them on
    GlassesOff,
    /// This is his normal face
    StraightFace,
    SlightSmile,
    Grimace,
    Surprise,
    Exhausted,
    Panicked,
}

impl Faces {
    /// Convert the faces to a short ascii representation
    pub fn to_ascii(&self) -> &'static str {
        match self {
            Faces::GlassesOff => "( •_•)",

            Faces::StraightFace => "(⎴▀_▀)",

            Faces::SlightSmile => "(⎴▀‿▀)",

            Faces::Grimace => "(⎴▀⁔▀)",

            Faces::Surprise => "(⎴▀.▀)",

            Faces::Exhausted => "( -_-)",

            Faces::Panicked => "( X‿X)",
        }
    }
}
