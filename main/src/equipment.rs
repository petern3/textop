use serde::{Deserialize, Serialize};

use crate::agent::Stats;

/// What skills the agent can upgrade
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
pub enum EquipmentType {
    /// Base Modifier
    Clothing,
    /// Close combat
    HandWeapon,
    /// Ranged combat
    Gun,
    /// Hot combat
    Explosive,
    /// [challenge bound] Machine combat
    Machine,
    /// Digital combat
    Computer,
    /// [location bound] Vehicle combat
    Vehicle,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "PascalCase")]
pub struct Equipment {
    pub name: String,
    pub category: EquipmentType,
    pub strength: i8,
    pub jump: i8,
    pub swimming: i8,
    pub running: i8,
    pub dodge: i8,
    pub stealth: i8,
    pub stealth_use: i8,
    pub modifier: i8,
}

impl Equipment {
    pub fn get_stats(&self, in_use: bool) -> Stats {
        let mut modifier: f32 = 0.0;
        let stealth: f32;
        if in_use {
            modifier = self.modifier.into();
            stealth = self.stealth_use.into();
        } else {
            stealth = self.stealth.into();
        }
        Stats {
            all: [
                self.strength.into(),
                self.jump.into(),
                self.swimming.into(),
                self.running.into(),
                self.dodge.into(),
                stealth,
                0.0,
                if self.category == EquipmentType::HandWeapon {
                    modifier
                } else {
                    0.0
                },
                if self.category == EquipmentType::Gun {
                    modifier
                } else {
                    0.0
                },
                if self.category == EquipmentType::Explosive {
                    modifier
                } else {
                    0.0
                },
                if self.category == EquipmentType::Machine {
                    modifier
                } else {
                    0.0
                },
                if self.category == EquipmentType::Computer {
                    modifier
                } else {
                    0.0
                },
                if self.category == EquipmentType::Vehicle {
                    modifier
                } else {
                    0.0
                },
            ],
        }
    }
}
