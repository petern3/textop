use std::error::Error;

mod databases;
mod gameplay;
mod interface;

mod agent;
mod bensford;
mod challenges;
mod encounters;
mod equipment;
mod locations;
mod messages;
mod player;
mod skills;

fn main() -> Result<(), Box<dyn Error>> {
    let mut datacore = databases::DataCore::new().expect("Failed to create databases");
    let mut inter = interface::Interface::new().expect("Failed to create interface");
    let mut gameplay = gameplay::Gameplay::new(&datacore).expect("Failed to create gameplay");

    let initial_messages = vec![
        messages::Message {
            time: chrono::Utc::now(),
            message: "I'm approaching the drop zone now".into(),
            agent_stats: messages::AgentStats {
                face: messages::Faces::GlassesOff,
                heartrate: 80,
                adrenaline: 20,
            },
        },
        messages::Message {
            time: chrono::Utc::now(),
            message: "The landing zone is in sight, I'm beginning the drop".into(),
            agent_stats: messages::AgentStats {
                face: messages::Faces::GlassesOff,
                heartrate: 80,
                adrenaline: 20,
            },
        },
    ];

    for message in initial_messages.iter() {
        datacore.add_message(&message);
    }

    // let encounter =
    //     encounters::Encounter::new(&datacore, None).expect("Failed to create encounter");
    // let encounter_1 = encounters::Encounter::new(&datacore, Some(&encounter))
    //     .expect("Failed to create encounter 1");
    // let encounter_2 = encounters::Encounter::new(&datacore, Some(&encounter_1))
    //     .expect("Failed to create encounter 2");

    // let mut agent = agent::Agent::new("Jim".to_string());

    // println!(
    //     "{} {} {:?}",
    //     &encounter.location.desc_en, &encounter.location_modifier, &encounter.challenges
    // );
    // println!(
    //     "{} {} {:?}",
    //     &encounter_1.location.desc_en, &encounter.location_modifier, &encounter_1.challenges
    // );
    // println!(
    //     "{} {} {:?}",
    //     &encounter_2.location.desc_en, &encounter.location_modifier, &encounter_2.challenges
    // );
    // println!(
    //     "{}, {:?}",
    //     datacore.get_location("complex_gate")?.name,
    //     datacore.get_location("complex_gate")?.category,
    // );
    // println!("{:?}", agent);
    // println!("{:?}", agent.get_stats());
    // println!("{}", datacore.get_skill("Ubuntu")?.name);
    // println!("{}", datacore.get_equipment("grenade")?.name);
    // println!("{:?}", datacore.get_skill_list()?);
    // println!("{:?}", datacore.get_equipment_list()?);

    loop {
        gameplay.update(&mut datacore);

        match inter.update(&mut datacore)? {
            interface::InterfaceAction::Quit() => {
                break;
            }
            interface::InterfaceAction::TrainSkill(skill) => {
                gameplay.train(&mut datacore, skill);
            }
            interface::InterfaceAction::GiveEquipment(equipment) => {
                gameplay.give_equipment(&mut datacore, equipment);
            }
            interface::InterfaceAction::None => {}
        };

        std::thread::sleep(std::time::Duration::from_millis(16));
    }

    inter.end()?;
    Ok(())
}
