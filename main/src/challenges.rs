use serde::{Deserialize, Serialize};

use crate::agent::{Agent, Stats};
use crate::bensford::get_bensford_number;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Challenge {
    pub name: String,
    pub min_stats: Stats,
    pub noun_en: String,
    pub verb_en: String,
}

impl Challenge {
    /// Returns -1 if can't complete, else it returns the index of the stat that won
    pub fn completion_stat_index(&self, agent: &Agent) -> i8 {
        let mut is_able_list = Vec::new();
        for index in (0..self.min_stats.all.len()).rev() {
            if self.min_stats.all[index] >= 0.0 {
                if agent.get_stats().all[index] >= self.min_stats.all[index] {
                    is_able_list.push(index as i8);
                }
            }
        }
        let mut stat_index: i8 = -1;
        if !is_able_list.is_empty() {
            let selected_index: u8 = get_bensford_number(is_able_list.len() as u8).unwrap() - 1;
            stat_index = is_able_list[selected_index as usize];
        }
        stat_index
    }

    pub fn able_to_complete(&self, agent: &Agent) -> bool {
        self.completion_stat_index(agent) >= 0
    }
}
