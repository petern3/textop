use std::ops;

use serde::{Deserialize, Serialize};

use crate::equipment::{Equipment, EquipmentType};
use crate::skills::Skill;

#[derive(Debug, Serialize, Deserialize)]
pub struct Stats {
    /// strength
    /// jump
    /// swimming
    /// running
    /// dodge
    /// stealth
    /// martial_art
    /// melee_combat
    /// gun_handling
    /// explosives_handling
    /// machine_operation
    /// computer_operation
    /// vehicle_operation
    pub all: [f32; 13],
}

impl Stats {
    pub fn new() -> Self {
        Self::zeros()
    }

    pub fn ones() -> Self {
        Self { all: [1.0; 13] }
    }

    pub fn zeros() -> Self {
        Self { all: [0.0; 13] }
    }

    pub fn apply_modifier(&self, modifier: Stats) -> Stats {
        let mut new_stats: [f32; 13] = [0.0; 13];
        // let base: f32 = 2.0;
        for index in 0..new_stats.len() {
            new_stats[index] = self.all[index] + modifier.all[index]
            // new_stats[index] = self.all[index] * base.powf(modifier.all[index])
        }
        Stats { all: new_stats }
    }
}

impl ops::Add for Stats {
    type Output = Stats;

    fn add(self, other: Stats) -> Stats {
        let mut new_stats: [f32; 13] = [0.0; 13];

        for index in 0..new_stats.len() {
            new_stats[index] = self.all[index] + other.all[index]
        }
        Stats { all: new_stats }
    }
}

impl ops::Mul for Stats {
    type Output = Stats;

    fn mul(self, other: Stats) -> Stats {
        let mut new_stats: [f32; 13] = [0.0; 13];
        for index in 0..new_stats.len() {
            new_stats[index] = self.all[index] * other.all[index]
        }
        Stats { all: new_stats }
    }
}

// impl Ord for Stats {
//     fn cmp(&self, other: &Self) -> Ordering {
//         self.all.cmp(&other.all)
//     }
// }

// impl PartialOrd for Stats {
//     fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
//         let mut ord = Ordering::Less;
//         for index in 0..self.all.len() {
//             if self.all[index] >= other.all[index] {
//                 ord = Ordering::Greater;
//             }
//         }
//         Some(ord)
//     }
// }

// impl PartialEq for Stats {
//     fn eq(&self, other: &Self) -> bool {
//         self.height == other.height
//     }
// }

// impl Eq for Stats {
//     fn eq(&self, other: &Self) -> bool {
//         let mut ord = Ordering::Less;
//         for index in 0..self.all.len() {
//             if self.all[index] > other.all[index] {
//                 ord = Ordering::Greater;
//             }
//         }
//         ord
//     }
// }

#[derive(Debug)]
pub struct Agent {
    pub name: String,
    pub skills: Vec<Skill>,
    pub equipment: Option<Equipment>,
    pub stats: Stats,
    pub equipment_in_use: bool,
}

impl Agent {
    pub fn new(name: String) -> Self {
        Self {
            name: name,
            skills: Vec::new(),
            equipment: None,
            stats: Stats::ones(),
            equipment_in_use: false,
        }
    }

    pub fn train(&mut self, skill: Skill) {
        self.skills.push(skill);
    }

    pub fn equip(&mut self, equipment: Option<Equipment>) {
        self.equipment = equipment;
    }

    pub fn equip_equipment(&mut self) {
        self.equipment_in_use = true;
    }

    pub fn unequip_equipment(&mut self) {
        self.equipment_in_use = false;
    }

    pub fn get_stats(&self) -> Stats {
        let base = &self.stats;
        let mut statmod: Stats = Stats::new();
        for skill in self.skills.iter() {
            statmod = statmod + skill.get_stats();
        }
        if self.equipment_in_use {
            match &self.equipment {
                Some(equipment) => {
                    statmod = statmod + equipment.get_stats(self.equipment_in_use);
                    statmod = statmod
                        * match equipment.category {
                            EquipmentType::HandWeapon => Stats {
                                all: [
                                    1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0,
                                ],
                            },
                            EquipmentType::Gun => Stats {
                                all: [
                                    1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0,
                                ],
                            },
                            EquipmentType::Explosive => Stats {
                                all: [
                                    1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0,
                                ],
                            },
                            EquipmentType::Machine => Stats {
                                all: [
                                    1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0,
                                ],
                            },
                            EquipmentType::Computer => Stats {
                                all: [
                                    1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0,
                                ],
                            },
                            EquipmentType::Vehicle => Stats {
                                all: [
                                    1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0,
                                ],
                            },
                            _ => Stats {
                                all: [
                                    1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0,
                                ],
                            },
                        }
                }
                None => {}
            };
        }

        base.apply_modifier(statmod)
    }
}
